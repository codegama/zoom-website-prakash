<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use DB, Log, Hash, Validator, Exception, Setting;

use App\Repositories\PaymentRepository as PaymentRepo;

use App\Repositories\CommonRepository as CommonRepo;

use App\Helpers\Helper;

use App\User, App\UserCard;

class MeetingApiController extends Controller
{
    protected $loginUser;

    protected $skip, $take;

	public function __construct(Request $request) {

        Log::info(url()->current());

        Log::info("Request Data".print_r($request->all(), true));
        
        $this->loginUser = User::CommonResponse()->find($request->id);

        $this->skip = $request->skip ?: 0;

        $this->take = $request->take ?: (Setting::get('admin_take_count') ?: TAKE_COUNT);

        $this->timezone = $this->loginUser->timezone ?? "America/New_York";

    }

    /** 
     * @method meetings_index()
     *
     * @uses Users Meetings History
     *
     * @created Bhawya N
     *
     * @updated Vithya R
     *
     * @param
     * 
     * @return JSON response
     *
     */

    public function meetings_index(Request $request) {

        try {

            $meetings = \App\Meeting::where('meetings.user_id', $request->id)
                                ->CommonResponse()
                                ->orderBy('meetings.updated_at' , 'desc')
                                ->skip($this->skip)->take($this->take)
                                ->get();

            return $this->sendResponse('', '', $meetings);
         
        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }

    }

    /** 
     * @method meetings_members()
     *
     * @uses meeting members list
     *
     * @created Vidhya R
     *
     * @updated Vidhya R
     *
     * @param
     * 
     * @return json response
     *
     */

    public function meetings_members(Request $request) {

        try {

            // Validation start

            $rules = ['meeting_unique_id' => 'required'];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);
            
            // Validation end

            $meeting = \App\Meeting::where('unique_id', $request->meeting_unique_id)->first();

            if(!$meeting) {
                
                throw new Exception(api_error(137), 137);
                
            }

            $meeting_members = \App\MeetingMember::where('meeting_id', $meeting->id)->CommonResponse()->get();

            $data['meeting_details'] = $meeting;

            $data['meeting_members'] = $meeting_members;

            return $this->sendResponse($message = "", $code = "", $data);
         
        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }

    }

    /** 
     * @method meetings_limit_chck()
     *
     * @uses start the meeting
     *
     * @created Vidhya R
     *
     * @updated Vidhya R
     *
     * @param
     * 
     * @return JSON response
     *
     */

    public function meetings_limit_check(Request $request) {

        try {

            $check_meeting = \App\Meeting::where('user_id', $request->id)->where('status', '!=', MEETING_COMPLETED)->first();
            
            if($check_meeting) {
                
                // throw new Exception(api_error(136), 136);
                
            }

            // Check the user has limits

            $user = User::find($request->id);

            if(!$user) {
                throw new Exception(api_error(1002), 1002);
            }

            $eligibility_response = CommonRepo::meetings_eligibility_check($request->id)->getData();

            if($eligibility_response->success == false) {

                throw new Exception($eligibility_response->error, $eligibility_response->error_code);
                
            }

            $data = $eligibility_response->data;

            return $this->sendResponse(api_success(114), 114, $data);
         
        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }

    }

    /** 
     * @method meetings_start()
     *
     * @uses start the meeting
     *
     * @created Vidhya R
     *
     * @updated Vidhya R
     *
     * @param
     * 
     * @return JSON response
     *
     */

    public function meetings_start(Request $request) {

        try {

            DB::beginTransaction();

            // Validation start

            $rules = ['username' => 'required', 'meeting_name' => 'required'];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);
            
            $check_meeting = \App\Meeting::where('user_id', $request->id)->where('status', '!=', MEETING_COMPLETED)->first();
            
            if($check_meeting) {
                
                // throw new Exception(api_error(136), 136);
                
            }

            $eligibility_response = CommonRepo::meetings_eligibility_check($request->id)->getData();

            if($eligibility_response->success == false) {

                throw new Exception($eligibility_response->error, $eligibility_response->error_code);
                
            }

            // Validation end

            $meeting = new \App\Meeting;

            $meeting->user_id = $request->id ?? 0;

            $meeting->username = $request->username ?? rand();

            $meeting->meeting_name = $request->meeting_name ?? rand();

            $meeting->start_time = date("Y-m-d H:i:s");

            $meeting->unique_id = routefreestring($meeting->meeting_name);
            
            $meeting->status = MEETING_STARTED;

            $meeting->save();

            $meeting->unique_id = routefreestring($meeting->meeting_name.'-'.rand(0, 10000).$meeting->id);

            $meeting->save();

            $free_user_call_time_limit = Setting::get('free_user_call_time_limit', 100);

            $free_user_call_members_limit = Setting::get('free_user_call_members_limit', 20);

            $total_duration_allowed_in_minutes = $eligibility_response->data->total_duration_allowed_in_minutes ?? $free_user_call_time_limit;

            $no_of_users = $eligibility_response->data->no_of_users ?? $free_user_call_members_limit;

            $bbb_response = \App\Repositories\BBBRepository::bbb_meeting_create($request, $meeting)->getData();

            if($bbb_response->success == false) {

                throw new Exception($bbb_response->error, $bbb_response->error_code);
            }

            $meeting->bbb_internal_meeting_id = $bbb_response->data->internalMeetingID ?? "";

            $meeting->bbb_voice_bridge = $bbb_response->data->voiceBridge ?? "";

            $meeting->bbb_dial_number = $bbb_response->data->dialNumber ?? "";

            $meeting->save();

            $attendeePW = $moderatorPW = '';

            if($request->id == $meeting->user_id) {

                $moderatorPW = 'mp'.$meeting->meeting_id;

            } else {

                $attendeePW = 'ap'.$meeting->meeting_id;

            }

            $request->request->add(['username' => $request->username,'moderatorPW' => $moderatorPW, 'attendeePW' => $attendeePW]);

            $response = \App\Repositories\BBBRepository::bbb_meeting_join($request, $meeting)->getData();

            if($response->success == false) {

                throw new Exception($response->error, $response->error_code);
            }

            DB::commit();

            $data = $eligibility_response->data;

            $data->meeting_id = $meeting->id ?? rand();

            $data->meeting_unique_id = $meeting->unique_id ?? rand();

            $data->username = $meeting->username ?? rand();

            $data->bbb_response = $bbb_response ?? [];

            $data->join_response = $response ?? [];

            \Log::info("bbb_response".print_r($bbb_response, true));

            return $this->sendResponse(api_success(112), 112, $data);
         
        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }

    }

    /** 
     * @method meetings_owner_update()
     *
     * @uses start the meeting
     *
     * @created Vidhya R
     *
     * @updated Vidhya R
     *
     * @param
     * 
     * @return JSON response
     *
     */

    public function meetings_connection_update(Request $request) {

        try {

            DB::beginTransaction();

            // Validation start

            $rules = ['connection_id' => 'required', 'meeting_id' => 'required|exists:meetings,id,user_id,'.$request->id];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);
            
            // Validation end

            $meeting = \App\Meeting::where('meetings.user_id', $request->id)->where('meetings.id' , $request->meeting_id)->first();

            if($meeting) {

                $meeting->connection_id = $request->connection_id ?? $meeting->unique_id;

                $meeting->save();
            }

            DB::commit();

            $data['meeting_id'] = $meeting->id ?? rand();

            $data['meeting_unique_id'] = $meeting->unique_id ?? rand();

            $data['username'] = $meeting->username ?? rand();

            $data['connection_id'] = $meeting->connection_id ?? rand();

            return $this->sendResponse(api_success(112), 112, $data);
         
        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }

    }

    /** 
     * @method meetings_members_join()
     *
     * @uses start the meeting
     *
     * @created Vidhya R
     *
     * @updated Vidhya R
     *
     * @param
     * 
     * @return JSON response
     *
     */

    public function meetings_members_join(Request $request) {

        try {

            DB::beginTransaction();

            // Validation start

            $rules = ['meeting_unique_id' => 'required'];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);
            
            // Validation end

            $meeting = \App\Meeting::where('unique_id', $request->meeting_unique_id)->where('status', '!=', MEETING_COMPLETED)->first();
            
            if(!$meeting) {

                throw new Exception(api_error(137), 137);
            }

            // Check the subscription limit

            $user_subscription_payment = \App\UserSubscription::where('user_id', $meeting->user_id)->where('is_current_subscription', YES)->first();

            $no_of_users_allowed = $user_subscription_payment->no_of_users ?? 4;

            $current_users = \App\MeetingMember::where('meeting_id', $meeting->id)->where('status', MEETING_MEMBER_JOINED)->count();

            if($current_users >= $no_of_users_allowed) {

                throw new Exception(api_error(138), 138);
                
            }

            $eligibility_response = CommonRepo::meetings_eligibility_check($meeting->user_id)->getData();

            if($eligibility_response->success == false) {

                throw new Exception($eligibility_response->error, $eligibility_response->error_code);
                
            }

            $member = \App\MeetingMember::where('meeting_id', $meeting->id)->where('user_id', $request->id)->first() ?? new \App\MeetingMember;

            $member->user_id = $request->id ?? 0;

            $member->username = $request->username ?? rand();

            $member->meeting_id = $meeting->id ?? rand();

            $member->start_time = date("Y-m-d H:i:s");

            $member->unique_id = routefreestring($member->meeting_name.strtotime($member->start_time));

            $member->status = MEETING_MEMBER_JOINED;

            $member->save();

            $meeting->no_of_users += 1;

            $meeting->save();

            $attendeePW = $moderatorPW = '';

            if($request->id == $meeting->user_id) {

                $moderatorPW = 'mp'.$meeting->meeting_id;

                $meeting->start_time = date('Y-m-d H:i:s');

                $meeting->status = MEETING_STARTED;

                $meeting->save(); 

            } else {

                $attendeePW = 'ap'.$meeting->meeting_id;

            }

            $request->request->add(['username' => $member->username,'moderatorPW' => $moderatorPW, 'attendeePW' => $attendeePW]);

            $response = \App\Repositories\BBBRepository::bbb_meeting_join($request, $meeting, $member)->getData();

            if($response->success == false) {

                throw new Exception($response->error, $response->error_code);
            }

            DB::commit();

            $data = $eligibility_response->data;

            $data->meeting_id = $meeting->id ?? rand();

            $data->meeting_unique_id = $meeting->unique_id ?? rand();

            $data->username = $member->username ?? "";

            $data->connection_id = $meeting->connection_id ?? rand();

            $data->join_response = $response;

            return $this->sendResponse(api_success(112), 112, $data);
         
        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }

    }

    /** 
     * @method meetings_end()
     *
     * @uses To End the Meetings
     *
     * @created Bhawya N
     *
     * @updated Bhawya N
     *
     * @param
     * 
     * @return JSON response
     *
     */

    public function meetings_end(Request $request) {

        try {

            DB::beginTransaction();

            // Validation start

            $rules = ['meeting_unique_id' => 'required|exists:meetings,unique_id'];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);
            
            $meeting = \App\Meeting::where('unique_id', $request->meeting_unique_id)->where('status', '!=', MEETING_COMPLETED)
                ->first();

            if(!$meeting) {
                
                throw new Exception(api_error(137), 137);

            }

            // if($meeting->user_id == $request->id) {

                \App\MeetingMember::where('meeting_id', $meeting->id)->where('status', MEETING_MEMBER_JOINED)->update(['status' => MEETING_MEMBER_ENDED, 'end_time' => date("Y-m-d H:i:s")]);

                $meeting->end_time = date("Y-m-d H:i:s");

                $meeting->status = MEETING_COMPLETED;

                $meeting->call_duration = calculate_call_duration($meeting->start_time, $meeting->end_time);

                $meeting->save();

            // } else {
                
                \App\MeetingMember::where('meeting_id', $meeting->id)->where('user_id', $request->id)->update(['status' => MEETING_MEMBER_ENDED, 'end_time' => date("Y-m-d H:i:s")]);
            // }

            DB::commit();

            $data['meeting_id'] = $meeting->id ?? rand();

            $data['meeting_unique_id'] = $meeting->unique_id ?? rand();

            return $this->sendResponse(api_success(113), 113, $data);
         
        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }

    }

    /** 
     * @method meetings_status()
     *
     * @uses To Check the Meeting Status
     *
     * @created Bhawya N
     *
     * @updated Bhawya N
     *
     * @param
     * 
     * @return JSON response
     *
     */

    public function meetings_status(Request $request) {

        try {

            // Validation start

            $rules = [
                'meeting_unique_id' => 'required|exists:meetings,unique_id'
            ];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);
            

            $meeting = \App\Meeting::where('user_id', $request->id)
                ->where('id', $request->meeting_id)
                ->first();

            if(!$meeting) {
                
                throw new Exception(api_error(137), 137);
                
            }

            $data['meeting_id'] = $meeting->id ?? rand();

            $data['meeting_unique_id'] = $meeting->unique_id ?? rand();

            $data['username'] = $meeting->username ?? rand();

            $data['status'] = $meeting->status;

            $data['status_text'] = meeting_status($meeting->status);

            return $this->sendResponse('', '', $data);
         
        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }

    }

    /** 
     * @method meetings_view()
     *
     * @uses Meeting Single View
     *
     * @created Bhawya N
     *
     * @updated Bhawya N
     *
     * @param
     * 
     * @return JSON response
     *
     */

    public function meetings_view(Request $request) {

        try {

            // Validation start

            $rules = [
                'meeting_unique_id' => 'required|exists:meetings,unique_id,user_id,'.$request->id,
            ];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);
            
            $meeting = \App\Meeting::where('unique_id', $request->meeting_unique_id)->where('user_id', $request->id)->CommonResponse()->first();

            if(!$meeting) {
                
                throw new Exception(api_error(137), 137);
                
            }

            $data['meeting_details'] = $meeting;

            $data['meeting_members'] = \App\MeetingMember::where('meeting_id', $meeting->id)->CommonResponse()->get();

            return $this->sendResponse($message = "", $code = "", $data);
         
        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }

    }

        /**
     * @method meetings_users_logout()
     *
     * @uses the other users logout @todo this function not completed, because not able to identify which user is logging out from BBB
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function meetings_users_logout(Request $request) {

        try {

            DB::beginTransaction();

            // Validation start

            $rules = ['meeting_id' => 'required|exists:meetings,unique_id'];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            // Validation end

            $meeting = \App\Meeting::where('unique_id', $request->meeting_id)->first();

            if(!$meeting) {
                throw new Exception(api_error(139), 139);
            }

            $meeting_member = \App\MeetingMember::find($request->meeting_member_id);

            if($meeting_member) {

                $meeting_member->status = MEETING_MEMBER_ENDED;

                $meeting_member->save();

                if($meeting_member->user_id == $meeting->user_id) {

                    $meeting->status = MEETING_COMPLETED;

                    $meeting->save();
                }
            }

            DB::commit();

            $frontend_url = Setting::get('frontend_url').'meetings-history';

            return redirect()->away($frontend_url);

        } catch(Exception $e) {

            DB::rollback();

            $frontend_url = Setting::get('frontend_url').'meetings-history';

            return redirect()->away($frontend_url);
        
        }
    
    }


}
