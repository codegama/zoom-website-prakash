@extends('layouts.admin')

@section('page_header',tr('meetings'))

@section('breadcrumbs')

<li class="breadcrumb-item"><a href="{{route('admin.meetings.index')}}">{{tr('meetings')}}</a></li>

<li class="breadcrumb-item active"><a href="javascript:void(0)"></a>{{tr('view_meetings')}}</li>
@endsection

@section('content')

<div class="card">

	<div class="card-header bg-info"> 

		<h4 class="m-b-0 text-white">{{tr('view_meetings')}}</h4>

	</div>

    <div class="card-body">

    	<div class="row">


            <div class="col-8">

                <!-- Card content -->
                <div class="card-body">

                    <div class="template-demo">

                        <table class="table mb-0">

                          <thead>
                           
                          </thead>

                          <tbody>

                            <tr>
                                <td class="pl-0"><b>{{ tr('meeting_name') }}</b></td>
                                <td class="pr-0 text-right"><div >{{$meeting_details->meeting_name}}</div></td>
                            </tr>

                            <tr>
                                <td class="pl-0"><b>{{ tr('name') }}</b></td>
                                <td class="pr-0 text-right"><div >
                                <a  href="{{ route('admin.users.view', ['user_id' => $meeting_details->user_id]) }}">{{$meeting_details->user_details->username ?? tr('not_available')}}</a></div></td>
                            </tr>

                            <tr>
                                <td class="pl-0"><b>{{ tr('no_of_users') }}</b></td>
                                <td class="pr-0 text-right"><div >{{$meeting_details->no_of_users}}</div></td>
                            </tr>
                        
                            <tr>
                                <td class="pl-0"><b>{{ tr('start_time') }}</b></td>
                                <td class="pr-0 text-right"><div >{{ common_date($meeting_details->start_time, Auth::guard('admin')->user()->timezone,'H:i:s') }}</div></td>
                            </tr>

                            <tr>
                                <td class="pl-0"><b>{{ tr('end_time') }}</b></td>
                                <td class="pr-0 text-right"><div >{{ common_date($meeting_details->end_time, Auth::guard('admin')->user()->timezone,'H:i:s') }}</div></td>
                            </tr>


                            <tr>

                              <td class="pl-0"> <b>{{ tr('status') }}</b></td>

                              <td class="pr-0 text-right">

                                    @if($meeting_details->status == MEETING_INITIATED)

                                        <span class="card-text label label-rouded label-menu label-warning">{{tr('meeting_initiated')}}</span>

                                    @elseif($meeting_details->status == MEETING_STARTED)

                                        <span class="card-text  label label-rouded label-menu label-success">{{tr('meeting_started')}}</span>

                                    @else

                                        <span class="card-text label label-rouded label-menu label-danger">{{tr('meeting_completed')}}</span>

                                    @endif

                              </td>

                            </tr>


                            <tr>
                                <td class="pl-0"> <b>{{ tr('created_at') }}</b></td>
                                <td class="pr-0 text-right"><div>{{ common_date($meeting_details->created_at, Auth::guard('admin')->user(), 'H:i:s') }}</div></td>
                            </tr>

                            <tr>
                                <td class="pl-0"> <b>{{ tr('updated_at') }}</b></td>
                                <td class="pr-0 text-right"><div>{{ common_date($meeting_details->updated_at,Auth::guard('admin')->user(),'H:i:s') }}</div></td>
                            </tr>

                          </tbody>

                        </table>

                    </div>

                </div>

                    <div class="grid-margin stretch-card"></div>

                    <div class="text-uppercase mb-3">
                        <h5>{{tr('meeting_members')}}</h5>
                    </div>

                    <div class="card card-shadow-remove">
                            <div class="card-body">
                                    @if(count($meeting_details->members) <= 0)
                                      <h4>{{tr('no_members_found')}}</h4>
                                    @else
                                    <ul class="list-arrow">
                                    @foreach($meeting_details->members as $key=>$members)
                                        <li><h5>{{$key+1}}.  {{$members->username}}</h5></li>&nbsp;
                                    @endforeach
                                   </ul>
                                   @endif
                                </div>
                        </div>		

            </div>


           
								
	            				
               
					            
	            			

        </div>

    </div>

</div>

@endsection

